var loginModel = require('../model/loginModel');

module.exports = function(app,con){
    
//Route for login Page
     app.get('/admin',function(req,res){
         res.render('login');
     });

  //Routing for login check
    app.post('/admin/login',function(req,res){
        req.checkBody('username','Username cannot be null').notEmpty();
        req.checkBody('password','Password cannot be empty').notyEmpty();
        var errors = req.validatorErrors();

        if(errors){
            res.json({
                status : false,
                message : errors
            });
            return;
        }
        else {
            loginModel.checkLogin([[
                req.body.username,
                req.body.password
            ]],con,function(success){
                if(success.length>0){
                  res.json({
                      status : true,
                      message : success
                  })
                }
            })
        }
    })   
}