//LOad env file

require('dotenv').config();

var mysql = require('mysql');

//Import Connection File to Connect to Db
var connection = require('./connection.js');

// Calling Method For DB Connection
var con = connection.getDbConnectionString();

con.connect((err) => {
    if(err){
         console.log('Connection Failed');
    }
    else {
        console.log('Connetcion established');
    }
});

//Loading packages
var express = require('express');
var app = new express();
var path = require('path');
var session = require('express-session');
var bodyParser = require('body-parser');
var validator =require('express-validator');

//Setting Middleware
app.use('/public',express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(validator());

var loginController = require('./controller/loginController');

app.use(session({
    secret: 'sweta',
    resave: true,
    saveUninitialized: true
}));

//Setting EJS as View Engine
app.set('view engine','ejs');

loginController(app,con);

//Settuing up ports
var port =	process.env.DB_PORT || 3001;

app.listen(port,function(){
		console.log("Server is Listening")
});
