/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  KeyboardAvoidingView,
  AsyncStorage,
  Dimensions,
  ToastAndroid
} from 'react-native';

import {StackNavigator} from 'react-navigation';

import { Container,
         Header,
         Content, 
         Item, 
         Input,
         InputGroup,
         Button 
        } from 'native-base';


export default class App extends Component {
  constructor(props) {
   super(props);
   this.state = {
       username : '',
       password : '',
       checkLogin : '',
       showPass : true,
       showPassColor : 'green',
       showPassText : 'SHOW'

   }
  }
    
   changePwdType = () => {
           if(this.state.showPass === true){         
           this.setState({ showPass : false, showPassColor : 'red', showPassText : 'HIDE '});
           }
           else{
           this.setState({ showPass : true , showPassColor : 'green', showPassText: 'SHOW '});
           }
           };
    login = async() =>{
     ToastAndroid.showWithGravity('Testing',ToastAndroid.CENTER,ToastAndroid.LONG);
    }
  render() {
    return (
        <ImageBackground source = {require('../assets/loginbackground.png')} style = {styles.back}>
           <KeyboardAvoidingView style={styles.wrapper}>
           <InputGroup style = {styles.textbox}>
           <Input
            value={this.state.username}
            onChangeText = {(username) =>this.setState({username})} 
            underlineColorAndroid = 'transparent'
            placeholder = 'Mobile/Email'
            />
           </InputGroup>
          <InputGroup style = {styles.textbox}>
           <Input
            value={this.state.password}
            onChangeText = {(password) =>this.setState({password})} 
            underlineColorAndroid = 'transparent'
            placeholder = 'Password'
            secureTextEntry = { this.state.showPass }
            />
            <Text
            style={{ color : this.state.showPassColor, fontWeight : 'bold' }}
            onPress = {this.changePwdType}
            >
            {this.state.showPassText}
            </Text>
           </InputGroup>
            <Button block info 
            style = {styles.loginButton}
            onPress = {this.login}
            >
            <Text style = {styles.loginButtonText}>LOGIN</Text>
            </Button>
           </KeyboardAvoidingView>
            </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
    back : {
        flex:1,
        width:Dimensions.get('window').width,
        height:Dimensions.get('window').height,
        alignItems:'center',
        justifyContent:'center'
    },
    wrapper :{
        position:'absolute',
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    textbox : {
        width : 320,
        height : 60,
        alignSelf : 'stretch',
        padding : 16,
        marginBottom : 5,
        backgroundColor : '#fff'
    },
    loginButton :{
        width : 320,
        height : 50,
        marginTop : 10,
    },
    loginButtonText : {
        fontWeight : 'bold'
    }
})